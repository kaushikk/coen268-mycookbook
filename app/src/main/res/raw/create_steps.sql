CREATE TABLE 'STEPS' (
	'STEPS_ID'	INTEGER PRIMARY KEY AUTOINCREMENT,
	'RECIPE_ID'	INTEGER,
	'TIME_TAKEN' INTEGER,
	'DESCRIPTION'	TEXT,
	FOREIGN KEY('RECIPE_ID') REFERENCES RECIPE(RECIPE_ID)
);