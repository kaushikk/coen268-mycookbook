package coen268.mycookbook;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kaushik on 5/19/15.
 */
public class Recipe {
    private long id;
    private String title;
    private Date lastUpdated;
    private String description;

    private List<Ingredient> ingredients;

    private List<CookingStep> cookingSteps;

    public Recipe(String title, String description) {
        this.title = title;
        this.description = description;
        ingredients = new ArrayList<>();
        cookingSteps = new ArrayList<>();
    }

    public Recipe(long id, String title, String description) {
        this(title,description);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<CookingStep> getCookingSteps() {
        return cookingSteps;
    }

    public void setCookingSteps(List<CookingStep> cookingSteps) {
        this.cookingSteps = cookingSteps;
    }

    public void addIngredient(Ingredient i ) {
        this.ingredients.add(i);
    }

    public void addCookingStep(CookingStep s) {
        this.cookingSteps.add(s);
    }
}
