package coen268.mycookbook;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AppKeyPair;

import org.rendersnake.HtmlCanvas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static coen268.mycookbook.Constants.KEY_RECIPE_ID;
import static coen268.mycookbook.Constants.KEY_SESSION_ID;
import static coen268.mycookbook.Constants.KEY_STEP_ID;
import static org.rendersnake.HtmlAttributesFactory.*;
/**
 * Created by kaushik on 5/13/15.
 */
public class Util extends Application {

    public static final String EXT_COOKBOOK_DIR = "MyCookbookCoen268";
    private static Util ctx;

    private String DROPBOX_APP_KEY;
    private String DROPBOX_APP_SECRET;
    private SharedPreferences prefs;

    @Override
    public void onCreate() {
        super.onCreate();
        ctx = this;
//        DROPBOX_APP_KEY = getResources().getString(R.string.dropbox_app_key);
//        DROPBOX_APP_SECRET = getResources().getString(R.string.dropbox_app_secret);
        DROPBOX_APP_KEY = "69j6cc74qi3rj09";
        DROPBOX_APP_SECRET = "qtbc5tb7dr61sco";
        prefs = getSharedPreferences("master",MODE_PRIVATE);
    }

    public static Util getInstance() {
        return ctx;
    }
    public String captureImage(Activity activity,File basePath,  String filename) {
        try {
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String filePath="";
            if (i.resolveActivity(activity.getPackageManager()) != null) {
                File photoFile = File.createTempFile(filename + System.currentTimeMillis(), ".jpg", basePath);
                filePath =  photoFile.getAbsolutePath();
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                activity.startActivityForResult(i, Constants.CAPTURE_IMAGE);
            }
            return filePath;
        }catch(IOException e) {
            Log.e("captureImage",e.getMessage());
            return null;
        }

    }

    public DropboxAPI getDropboxApi() {
        DropboxAPI<AndroidAuthSession> dropboxAPI;
        AppKeyPair appKeys = new AppKeyPair(DROPBOX_APP_KEY,DROPBOX_APP_SECRET);
        AndroidAuthSession  session = new AndroidAuthSession(appKeys);
        dropboxAPI = new DropboxAPI<AndroidAuthSession>(session);
        return dropboxAPI;
    }

    // CALL THIS ON onResume() of the activity
    public String getDropBoxToken(DropboxAPI<AndroidAuthSession> api) {
        String accessToken=null;
        if(prefs.contains("dropbox_token")) {
            accessToken = prefs.getString("dropbox_token",null);
        }
        //obtain access token
        return accessToken;
    }

    public void addDropboxToken(String accessToken) {
        if(!prefs.contains("dropbox_token")) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("dropbox_token", accessToken);
            editor.commit();
        }
    }

    public static Bundle getDefaultBundle(Long recipeId,Long sessionId,  Long stepId ) {
        Bundle b = new Bundle();
        b.putLong(KEY_SESSION_ID,sessionId);
        b.putLong(KEY_STEP_ID,stepId);
        b.putLong(KEY_RECIPE_ID,recipeId);
        return b;
    }

    public Map<String,Object> getReportData(Context c, CookingSession session) {
        Dao dao = Dao.getInstance(c);
        long recipeId = session.getParentRecipeId();

        Recipe r = dao.getRecipeById(recipeId);
        List<SessionStepInfo> sessionSteps = dao.getSessionSteps(session.getSessionId());
        Map<Long,List<SessionStepInfo>> map =  sortSessionSteps(sessionSteps);
        HtmlCanvas canvas = new HtmlCanvas();
        List<String> imgLoc = new ArrayList<>();
        try {
             canvas.html().head().
                     title().content("My Cook Book")
                     .style().content(
                     "td{border: 1px solid black}" +
                     "tr{border: 1px solid black}"+
                     "img{width: 100px; height: 100px; padding: 10px; }" +
                     "#content {\n" +
                             "position: relative;\n" +
                             "height: 1000px;\n" +
                             "width: 600px;\n" +
                             "padding-left: 6%;\n" +
                             "padding-right: 5%;\n" +
                             "padding-top: 2%;\n" +
                             "background-image: url('bg_cookbook.jpg');\n" +
                             "background-size: contain;\n" +
                             "background-repeat: no-repeat;\n" +
                     "}\n" +
                     "body {\n" +
                             "width: 1024px;\n" +
                             "height: 100%;\n" +
                             "position: relative;\n" +
                     "}\n" +
                     ".title {\n" +
                             "display: inline;\n" +
                             "font-family: cursive;\n" +
                             "color: white;\n" +
                             "font-stretch: expanded;\n" +
                             "padding-bottom: 30px;\n" +
                             "padding-top: 10px;" +
                             "padding-left: 10px;" +
                             "white-space: pre-wrap; " +
                     "}\n" +
                     ".cursive {" +
                             "font-family: cursive;\n" +
                             "color: black;\n" +
                             "font-stretch: expanded;\n" +
                     "}\n"
             )
                     ._head()
                     .body().div(id("content"))
                     .h1(class_("title")).content(r.getTitle())
                     .p().content(r.getDescription()).br()
                     .h2(class_("cursive")).content("Ingredients");
             canvas.table();
            for(Ingredient ing: r.getIngredients()) {
                canvas.tr().td().content(ing.getName())
                        .td().content(ing.getQuantity())
                        .tag_close("tr");
            }
            canvas._table();
            canvas.br();
            canvas.h2(class_("cursive")).content("Steps");
            canvas.ol();
//            canvas.div(style("width: 900px; height: 50px; word-wrap: break-word; overflow: auto;"));
            for(CookingStep s: r.getCookingSteps()) {
                StringBuilder b = new StringBuilder();

                b.append(s.getDescription());
                if(s.getTimeTaken()>0) {
                    b.append(" ( ").append(s.getTimeTaken() + " Minutes").append(')');
                }
                long stepId = s.getId();
                canvas.li(style("padding-right: 10%")).content(b.toString());

//                if(s.getTimeTaken()>0) {
//                    canvas.p(style("text-align: right; padding-right: 20%")).i().content(s.getTimeTaken() + " Minutes")._p();
//                }
                if(map.containsKey(stepId)) {
                    List<SessionStepInfo> l = map.get(stepId);
                    String str=new String("Note:");
                    boolean notePresent=false;
                    List<String> notes = new ArrayList<>();
                    for(SessionStepInfo stepInfo: l) {
                        if(stepInfo.getType()== SessionStepInfo.Type.TEXT_NOTE) {
                            notes.add(stepInfo.getFileLocation());
                            notePresent=true;
                        }
                    }
                    if(notePresent && notes.size()>0 ) {
                        canvas.div().i().b().content("Notes: ")._i()._div();
                        canvas.ul();
                        for (String note : notes) {
                            canvas.li().i().content(note)._li();
                        }
                        canvas._ul();
                    }

                    for(SessionStepInfo stepInfo: l) {
                        if(stepInfo.getType()== SessionStepInfo.Type.IMAGE) {
                            String file = stepInfo.getFileLocation();
                            String fileName = file.substring(file.lastIndexOf("/")+1, file.length());
                            imgLoc.add(file);
                            canvas.img(src(fileName));
//                            canvas.a(href(fileName)).img(src(fileName)).style().content("width: 100px; height: 100px;")._a();
//                            canvas.div(style("font-style: italic;")).content("IMG PATH: " + file);
                        }
                    }

                }
            }
//            canvas._div();
            canvas._ol();
            canvas._div();
            canvas._body();
            canvas._html();
            Map<String, Object> output = new HashMap<>();
            output.put("html",canvas.toHtml());
            output.put("images",imgLoc);
            return output;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Map<Long, List<SessionStepInfo>> sortSessionSteps(List<SessionStepInfo> sessionSteps) {
        Map<Long, List<SessionStepInfo>> map = new HashMap();
        for(SessionStepInfo s: sessionSteps) {
            long stepId = s.getStepId();
            if(!map.containsKey(stepId)) {
                List<SessionStepInfo> a = new ArrayList<>();
                map.put(stepId,a);
            }
            map.get(stepId).add(s);
        }
        return map;
    }

    public String copyData( Map<String,Object> map, String destDir) throws Exception {
        File f =  getExternalFilesDir(EXT_COOKBOOK_DIR);
        File outDir = new File(f,destDir);
        if(outDir.exists()) {
            outDir.delete();
        }
        outDir.mkdir();

        //create HTML File
        File htmlFile= new File(outDir,"index.html");
        htmlFile.createNewFile();
        FileWriter w = new FileWriter(htmlFile);
        w.write(map.get("html").toString());
        w.flush();
        w.close();



        FileInputStream src;
        FileOutputStream dest;

        List<String> files = (List<String>) map.get("images");

        for(String file: files) {
            src = new FileInputStream(file);
            String fileName = file.substring(file.lastIndexOf("/")+1, file.length());
            File outFile = new File(outDir, fileName);
            dest = new FileOutputStream(outFile);
            byte[] b = new byte[1024];
            while(src.read(b)!=-1) {
                dest.write(b);
            }
            src.close();
            dest.flush();
            dest.close();
        }

        //Copy Background image

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.bg_cookbook);
        File outFile = new File(outDir, "bg_cookbook.jpg");
        dest = new FileOutputStream(outFile);
        bm.compress(Bitmap.CompressFormat.PNG, 100, dest);
        dest.flush();
        dest.close();

        return outDir.getAbsolutePath();
    }
}
