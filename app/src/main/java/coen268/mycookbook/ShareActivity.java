package coen268.mycookbook;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

//import static coen268.mycookbook.SharedModel.selectedRecipe;

/**
 * Created by ramyajagannath on 5/22/15.
 */
public class ShareActivity extends Activity implements DropboxListener {
    ShareDialog shareDialog;
    String newFolderName;
    private CookingSession session;
    private Dao dao;
    private Recipe selectedRecipe;
    private CallbackManager callbackManager;
    private String shareType;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        shareDialog = new ShareDialog(this);

        Intent i = getIntent();
        long sessionId = i.getLongExtra("sessionId", -1);
        shareType = i.getStringExtra("SHARE_TYPE");

         session=null;
        Util u = Util.getInstance();
        dao = Dao.getInstance(this);
        if(sessionId >0) {
            List<CookingSession> sessionById = dao.getSessionById(sessionId);
            if(sessionById!=null && sessionById.size()>0) {
                session = sessionById.get(0);
                selectedRecipe = dao.getRecipeById(session.getParentRecipeId());
            }
        }else {
            session = SharedModel.getSessionReference();
            selectedRecipe = SharedModel.getSelectedRecipe();
        }

        if(session != null) {
            String filePath = "";
            newFolderName = session.getName();
            Map<String, Object> map = u.getReportData(this, session);
            String html = (String) map.get("html");
            try {
                filePath = u.copyData(map, newFolderName);
                /*FileOutputStream os = this.openFileOutput("out-" + System.currentTimeMillis() + ".html", Context.MODE_PRIVATE);
                FileWriter w = new FileWriter(os.getFD());
                w.write(html);
                w.flush();
                w.close();*/
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //System.out.println(html);
            DropboxAPI<AndroidAuthSession> api;
            api = u.getDropboxApi();
            if(api != null) {
                String token = u.getDropBoxToken(api);
                if (token == null)
                    api.getSession().startOAuth2Authentication(this);
                else {
                    api.getSession().setOAuth2AccessToken(token);
                }
                DropboxUploadFileTask task = new DropboxUploadFileTask(api, this);
                if (token != null) {
                    task.execute(new File(filePath));
//                    DropboxFileInfoTask fileInfoTask = new DropboxFileInfoTask(api, this);
//                    fileInfoTask.execute("/" + newFolderName, "index.html");
                }
            }
        }
        else
        {
            finish();
        }


        /*shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(getApplicationContext(), "You shared this post", Toast.LENGTH_SHORT).show();
                //finish();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "You did not share this post", Toast.LENGTH_SHORT).show();
                //finish();
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
                //finish();
            }
        });*/

    }

    @Override
    public void onDropboxResult(Object o) {
        if(o==null) {
            Toast.makeText(getApplicationContext(), "Unable to create directory in dropbox", Toast.LENGTH_SHORT).show();
        }
        if(o instanceof DropboxAPI.DropboxLink) {
            DropboxAPI.DropboxLink share = (DropboxAPI.DropboxLink) o;
            if(newFolderName != null) {
                String htmlPath = getResources().getString(R.string.dropbox_url) + newFolderName + "/index.html";
//                Toast.makeText(getApplicationContext(), "Shared Link is: " + htmlPath, Toast.LENGTH_SHORT).show();
                Log.d("DropBox Path", htmlPath);
                 dao = Dao.getInstance(this);

                String title = selectedRecipe.getTitle();
                String description = selectedRecipe.getDescription();
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent content = new ShareLinkContent.Builder()
                            .setContentTitle(title)
                            .setContentDescription(description)
                            .setContentUrl(Uri.parse(htmlPath))
                            .build();
                    shareDialog.show(content);
                    finish();
                }
            }
        }else if(o instanceof DropboxAPI.Entry) {
            if(newFolderName != null) {
                String htmlPath = getResources().getString(R.string.dropbox_url) + newFolderName + "/index.html";
                Toast.makeText(getApplicationContext(), "Shared Link is: " + htmlPath, Toast.LENGTH_SHORT).show();
                Log.d("DropBox Path", htmlPath);
                dao = Dao.getInstance(this);

                String title = selectedRecipe.getTitle();
                String description = selectedRecipe.getDescription();
                if(shareType.matches("all")) {
                    htmlPath = htmlPath.replaceAll(" ", "%20");
                    Toast.makeText(this, "Sharing post!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, title);
                    intent.putExtra(Intent.EXTRA_TITLE, title);
                    intent.putExtra(Intent.EXTRA_TEXT, "\n" + description + "\n" + htmlPath);
                    startActivity(Intent.createChooser(intent, "Share"));
                    finish();
                }
                else{
                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent content = new ShareLinkContent.Builder()
                                .setContentTitle(title)
                                .setContentDescription(description)
                                .setContentUrl(Uri.parse(htmlPath))
                                .build();

                        callbackManager = CallbackManager.Factory.create();
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                Toast.makeText(getApplicationContext(), "You shared this post", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onCancel() {
                                Toast.makeText(getApplicationContext(), "You did not share this post", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(FacebookException e) {
                                e.printStackTrace();
                            }
                        });
                        shareDialog.show(content);
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }
}
