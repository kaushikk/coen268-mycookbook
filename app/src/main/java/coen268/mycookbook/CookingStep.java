package coen268.mycookbook;

/**
 * Created by kaushik on 5/19/15.
 */
public class CookingStep {
    private long recipeId;
    private long id;
    private String description;
    private long timeTaken; // MINUTES

    public CookingStep(long recipeId, long id, String description) {
        this.recipeId = recipeId;
        this.id = id;
        this.description = description;
    }

    public CookingStep(String description, long timeTaken) {
        this.description = description;
        this.timeTaken = timeTaken;
    }

    public long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(long recipeId) {
        this.recipeId = recipeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }
}
