package coen268.mycookbook;

import java.util.List;

/**
 * Created by kaushik on 5/20/15.
 */
public class SharedModel {
    private static List<Recipe> recipeList;

    private static Recipe selectedRecipe;

    private static long recipeIndex;

    private static CookingSession sessionRef;

    public static List<Recipe> getRecipeList() {
        return recipeList;
    }

    public static void setRecipeList(List<Recipe> recipeList) {
        SharedModel.recipeList = recipeList;
    }

    public static Recipe getSelectedRecipe() {
        return selectedRecipe;
    }

    public static void setSelectedRecipe(Recipe selectedRecipe) {
        SharedModel.selectedRecipe = selectedRecipe;
    }

    public static long getRecipeIndex() {
        return recipeIndex;
    }

    public static void setRecipeIndex(int recipeIndex) {
        SharedModel.recipeIndex = recipeIndex;
        SharedModel.selectedRecipe = recipeList.get(recipeIndex);
    }

    public static Recipe getRecipeByIndex(int index) {
        return recipeList.get(index);
    }


    private static long sessionId;

    public static long getSessionId() {
        return sessionId;
    }

    public static void setSessionId(long sessionId) {
        SharedModel.sessionId = sessionId;
    }

    public static void setSessionReference(CookingSession session) {
        SharedModel.sessionRef = session;
    }

    public static CookingSession getSessionReference(){return sessionRef;}
}
