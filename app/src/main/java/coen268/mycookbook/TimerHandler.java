package coen268.mycookbook;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;

import java.util.Timer;


/**
 * Created by ramyajagannath on 5/22/15.
 */
public class TimerHandler {
    private CountDownTimer timer;
    Handler mHandler;

    public TimerHandler(Handler handler){
        mHandler = handler;
    }
    public void startTimer(long timeOutPeriod/** In minutes **/){
        timer = new CountDownTimer(timeOutPeriod*60*1000, 1000*60) {

            public void onTick(long millisUntilFinished) {
                Message msg = new Message();
                msg.what = 1;
                msg.arg1 = (int)millisUntilFinished/(60*1000);
                if(msg.arg1 > 0)
                    mHandler.dispatchMessage(msg);
            }

            public void onFinish() {
                Message msg = new Message();
                msg.what = 2;
                mHandler.dispatchMessage(msg);
            }
        }.start();

    }

    public void stopTimer(){
        if(this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
    }
}
