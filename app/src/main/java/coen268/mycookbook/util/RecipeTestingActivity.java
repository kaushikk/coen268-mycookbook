package coen268.mycookbook.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Date;

import coen268.mycookbook.Dao;
import coen268.mycookbook.ListofRecipe;
import coen268.mycookbook.R;
import coen268.mycookbook.Recipe;


public class RecipeTestingActivity extends Activity {

    private Dao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_testing);
        this.dao = Dao.getInstance(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createRecipe(View v) {
        Recipe r = new Recipe("COOK RICE","Instructions on how to cook rice");
        r.setLastUpdated(new Date(System.currentTimeMillis()));
        Recipe r1 = Dao.getInstance(this).createRecipe(r);
        Toast.makeText(this, "RECIPE CREATED " + r1.getId(), Toast.LENGTH_SHORT).show();
    }

    public void addRecipe(View v ) {
        MockRecipes.createRecipes(dao);
    }

    public void resetDb(View v ) {
        dao.resetDb();
    }

    public void listRecipes(View v) {
        Intent i = new Intent(this, ListofRecipe.class);
        startActivity(i);
    }
}
