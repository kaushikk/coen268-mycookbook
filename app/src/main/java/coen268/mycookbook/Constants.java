package coen268.mycookbook;

/**
 * Created by kaushik on 5/13/15.
 */
public interface Constants {
    public static int CAPTURE_IMAGE = 101;
    public static int REQ_CODE_UPLOAD_DROPBOX = 102;
    public static int REQ_CODE_TAKE_NOTE = 103;
    public static int CAPTURE_AND_UPLOAD = 104;

    public static String RECIPE_ID = "recipe_id";
    public static String RECIPE_TITLE = "title";
    public static String RECIPE_DESCRIPTION = "description";
    public static String RECIPE_LAST_UPDATED = "last_updated";

    public static String STEPS_ID = "steps_id";
    public static String STEPS_DESCRIPTION =  "description";
    public static String STEPS_PARENT_RECIPE =  "recipe_id";
    public static String STEPS_TIME_TAKEN =  "time_taken";

    String INGREDIENTS_ID = "ingredient_id";
    String INGREDIENTS_NAME = "name";
    String INGREDIENTS_QUANTITY = "quantity";
    String INGREDIENTS_IMG_LOC = "img_loc";
    String INGREDIENTS_PARENT_RECIPE = "recipe_id";

    String SESSION_IMG_ID = "session_id";
    String SESSION_IMG_STEP_ID = "step_id";
    String SESSION_IMG_SESSION_ID = "session_id";
    String SESSION_IMG_FILE_LOC = "file_loc";
    String SESSION_IMG_FILE_LOC_TYPE = "file_type";
    String SESSION_IMG_TYPE = "type";

    String SESSION_ID = "session_id";
    String SESSION_PARENT_RECIPE = "parent_recipe_id";
    String SESSION_NAME = "name";
    String SESSION_CREATED_DATE = "created_date";
    String SESSION_LAST_UPDATED = "last_updated";

    String FILE_LOC_LOCAL = "local";
    String FILE_LOC_SDCARD = "sdcard";
    String FILE_LOC_DROPBOX = "dropbox";

    String IMG_STORE_FOLDER="MyCookBook";

    String KEY_SESSION_ID="sessionId";
    String KEY_RECIPE_ID="recipeId";
    String KEY_STEP_ID="stepId";
    String KEY_IMG_PATH = "filepath";
    String KEY_IMG_FILENAME="filename";
    String KEY_IMG_STORE = "appdir";
    String KEY_CAPTURE_TIME="captureTime";

}
