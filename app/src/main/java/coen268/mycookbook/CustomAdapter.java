package coen268.mycookbook;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * Created by Mantu on 20-05-2015.
 */


    public class CustomAdapter extends ArrayAdapter<Recipe> {
        private final List<Recipe> recipelist;
    private final Activity activity;

    public CustomAdapter(Context context, int resource, List<Recipe> recipelist) {
            super(context, resource, recipelist);
            this.recipelist = recipelist;
            this.activity = (Activity)context;
        }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.customlayout, parent,false);

        TextView textView = (TextView) row.findViewById(R.id.RecipeName);
        Recipe recipe = recipelist.get(position);
        textView.setText(recipe.getTitle());

        ImageView image = (ImageView) row.findViewById(R.id.recipeImg);

        try {
            // get input stream

            InputStream ims = activity.getAssets().open(recipe.getTitle()+".JPG");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            image.setImageDrawable(d);
        }
        catch(IOException ex) {
            Log.e("Error Loading Image",ex.getMessage());
        }

//
//       textView = (TextView) row.findViewById(R.id.lastupdated);
//        Date lastUpdated = recipelist.get(position).getLastUpdated();
//        if(lastUpdated!=null)
//            textView.setText(lastUpdated.toString());
        //ImageView imageView = (ImageView) row.findViewById(R.id.rowImage);
        //int id = getContext().getResources().getIdentifier(person.get(position).getFilename(), "drawable", getContext().getPackageName());
        //imageView.setImageResource(id);

        return row;
    }
}

