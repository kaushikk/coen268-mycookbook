package coen268.mycookbook;

/**
 * Created by kaushik on 5/19/15.
 */
public class SessionStepInfo {
    public enum Type {IMAGE,TEXT_NOTE}
    private long sessionId;
    private long stepId;
    private String fileLocation;
    private FileLocation locationType;
    private Type type;
    private long id;

    public SessionStepInfo() {
        locationType= FileLocation.OTHER;
    }
    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public long getStepId() {
        return stepId;
    }

    public void setStepId(long stepId) {
        this.stepId = stepId;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public FileLocation getLocationType() {
        return locationType;
    }

    public void setLocationType(FileLocation locationType) {
        this.locationType = locationType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
