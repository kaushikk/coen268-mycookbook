package coen268.mycookbook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;

import java.io.File;
import java.io.IOException;

import static android.os.Environment.DIRECTORY_PICTURES;


import com.facebook.FacebookSdk;
import com.facebook.share.widget.ShareDialog;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.CallbackManager;
import com.facebook.share.Sharer;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.LikeView;

import coen268.mycookbook.util.RecipeTestingActivity;


public class TestActivity extends Activity implements Constants, DropboxListener{

    private String path;
    private DropboxAPI<AndroidAuthSession> api;
    private String token;
    private Util util;
    private File photoFile;
    private ShareDialog shareDialog;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        util = Util.getInstance();
        api = util.getDropboxApi();
        this.token = util.getDropBoxToken(api);
        if(token==null)
            api.getSession().startOAuth2Authentication(this);
        else {
            api.getSession().setOAuth2AccessToken(this.token);
        }

        /** FB initialization and call back **/
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(getApplicationContext(), "You shared this post", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "You did not share this post", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
            }
        });
        LikeView likeView = (LikeView) findViewById(R.id.fb_like_view);
        likeView.setObjectIdAndType(
                "https://www.facebook.com/FacebookDevelopers",
                LikeView.ObjectType.PAGE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void takePicture(View v){
        clickImage(CAPTURE_IMAGE);
    }

    private void clickImage(int requestCode) {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(i.resolveActivity(getPackageManager()) != null) {
            File f = Environment.getExternalStoragePublicDirectory(DIRECTORY_PICTURES);
            File appDir = new File(f, getApplicationInfo().getClass().getName());
            if(!appDir.exists())
                appDir.mkdir();
            try {
                this.photoFile = File.createTempFile("image_"+System.currentTimeMillis(),".jpg",appDir);
                this.path = photoFile.getAbsolutePath();
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(i, requestCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CAPTURE_IMAGE) {
            Bitmap bitmap = BitmapFactory.decodeFile(this.path);
            ImageView img = (ImageView)findViewById(R.id.imageView);
            img.setImageBitmap(bitmap);
        }else if(requestCode == CAPTURE_AND_UPLOAD) {
            uploadToDropbox(null);
        }

    }

    public void uploadToDropbox(View v) {
        try {
            String token = util.getDropBoxToken(api);
            DropboxUploadFileTask task = new DropboxUploadFileTask(api,this);
            if (token != null) {
               task.execute(photoFile);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void clickAndUpload(View v) {
        clickImage(CAPTURE_AND_UPLOAD);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AndroidAuthSession session = api.getSession();
        if (this.token==null && session.authenticationSuccessful()) {
            try {
                // Required to complete auth, sets the access token on the session
                session.finishAuthentication();
                this.token= session.getOAuth2AccessToken();
                util.addDropboxToken(token);
            } catch (IllegalStateException e) {
                Log.i("DbAuthLog", "Error authenticating", e);
            }
        }
    }
    
    /** FB methods **/
    public void shareLinkOnFB(View v) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentTitle("Hello Facebook")
                    .setContentDescription(
                            "The 'Hello Facebook' sample  showcases simple Facebook integration")
                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
                    .build();
            shareDialog.show(content);
        }
    }

    public void shareImageOnFB(View v) {
        if (ShareDialog.canShow(SharePhotoContent.class)) {
            Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(image)
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            shareDialog.show(content);
        }
    }
    
    public void likeOnFB(View v) {

    }

    public void createRecipe(View v ){
        startActivity(new Intent(TestActivity.this, RecipeTestingActivity.class));
    }

    public void testTimer(View v) {
        //TimerHandler timerHandler;
        //timerHandler = new TimerHandler(this);
        //timerHandler.startTimer(1);
    }

    public void uploadDirToDropbox(View v) {
        try {
            String token = util.getDropBoxToken(api);
            DropboxUploadFileTask task = new DropboxUploadFileTask(api,this);
            if (token != null) {
                File externalFilesDir = getExternalFilesDir(Util.EXT_COOKBOOK_DIR);
                task.execute(new File(externalFilesDir,"Session001"));
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDropboxResult(Object o) {

        if(o==null) {
            Toast.makeText(this, "NULL RESPONSE!!!!!", Toast.LENGTH_SHORT).show();
        }
        if(o instanceof DropboxAPI.DropboxLink) {
            DropboxAPI.DropboxLink share = (DropboxAPI.DropboxLink) o;
            Toast.makeText(this,"Shared Link is: "+ share.url,Toast.LENGTH_SHORT).show();
        }else if(o instanceof DropboxAPI.Entry) {
            DropboxAPI.Entry response = (DropboxAPI.Entry) o;
            if(response!=null && response.isDir) {
                Toast.makeText(this, "UPLOADED DIR: " + response.fileName(), Toast.LENGTH_SHORT).show();
            }else if(response !=null) {
                Toast.makeText(this, "UPLOADED FILE: " + response.rev, Toast.LENGTH_SHORT).show();
            }else {
                //do nothing
            }
        }
    }

    public void getSharedLink(View view) {
        DropboxFileInfoTask task = new DropboxFileInfoTask(api, this);
        task.execute("/Sample_Session","index.html");
    }
}
