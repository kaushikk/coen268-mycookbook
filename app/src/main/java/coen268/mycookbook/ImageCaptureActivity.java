package coen268.mycookbook;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import static coen268.mycookbook.Constants.*;
import static android.os.Environment.DIRECTORY_PICTURES;

/**
 * Created by kaushik on 5/26/15.
 */
public class ImageCaptureActivity extends Activity {

    private File photoFile;
    private String path;
    private long captureTime;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        this.bundle = getIntent().getExtras();
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String filename = bundle.getString(KEY_IMG_FILENAME);
        String dirLoc = bundle.getString(KEY_IMG_STORE);
        if(i.resolveActivity(getPackageManager()) != null) {
            File f = Environment.getExternalStoragePublicDirectory(DIRECTORY_PICTURES);
            File appDir = new File(f, dirLoc);
            if(!appDir.exists())
                appDir.mkdir();
            try {
                captureTime = System.currentTimeMillis();
                this.photoFile = File.createTempFile(filename , ".jpg", appDir);
                bundle.putLong(KEY_CAPTURE_TIME,captureTime);
                this.path = photoFile.getAbsolutePath();
                bundle.putString(KEY_IMG_PATH,path);
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                int requestCode= CAPTURE_IMAGE;
                startActivityForResult(i, requestCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAPTURE_IMAGE:
                if(resultCode == RESULT_OK) {
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
        }
    }
}
