package coen268.mycookbook;

/**
 * Created by kaushik on 5/28/15.
 */
public interface DropboxListener {
    public void onDropboxResult(Object o);
}
