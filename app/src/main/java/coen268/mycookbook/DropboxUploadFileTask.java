package coen268.mycookbook;

import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ramyajagannath on 5/14/15.
 */
public class DropboxUploadFileTask extends AsyncTask<Object,Integer, DropboxAPI.Entry> {

    public static final String DROPBOX_ROOT = "/Apps/Pancake.io/";
    private DropboxListener listener;
    private DropboxAPI<AndroidAuthSession> api;
    private Util util;

    public DropboxUploadFileTask(DropboxAPI<AndroidAuthSession> api, DropboxListener a) {
        this.api = api;
        this.listener = a;
        this.util = Util.getInstance();
    }

    @Override
    protected DropboxAPI.Entry doInBackground(Object... params) {
        File srcFile=null;
        String srcFileName=null;
        if(params[0] instanceof File) {
            srcFile = (File) params[0];
        }else if(params[0] instanceof String) {
            srcFileName =(String) params[0];
        }
        DropboxAPI.Entry response = null;
        long length=-1;
        String name=null;
        InputStream is=null;
        try {
            AssetManager assets = util.getAssets();
            boolean dir=false;
            DropboxAPI.Entry dirEntry = null;
            if(srcFile!=null) {
                if(srcFile.isDirectory()) {
                    dir=true;
                    try {
                        dirEntry = api.metadata(DROPBOX_ROOT + srcFile.getName(), 1, null, false, null);
                    }catch (DropboxException e) {
                        dirEntry = api.createFolder(DROPBOX_ROOT + srcFile.getName());
                    }
//                    if(dirEntry.rev==null) {
//                        throw new RuntimeException("Error Creating Directory");
//                    }
                    for(File f: srcFile.listFiles()) {
                        length=f.length();
                        name = f.getName();
                        FileInputStream fis = new FileInputStream(f);
                        api.putFileOverwrite(dirEntry.path+"/"+name,fis,length,null);
                        fis.close();

                    }
                }else {
                    is=new FileInputStream(srcFile);
                    length = srcFile.length();
                    name = srcFile.getName();
                }
            }else if(srcFileName!=null) {
                is = assets.open(srcFileName);
                length = assets.openFd(srcFileName).getLength();
                name=srcFileName;
            }else {
                return null;
            }
            if(!dir) {
                response = api.putFile(name, is, length, null, null);
            }else {
                response = dirEntry;
            }
        } catch (DropboxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//                    }
        return response;
    }

    @Override
    protected void onPostExecute(DropboxAPI.Entry response) {
       listener.onDropboxResult(response);
    }
}
