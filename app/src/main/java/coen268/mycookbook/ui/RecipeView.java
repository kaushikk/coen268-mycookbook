package coen268.mycookbook.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.Map;

import coen268.mycookbook.Constants;
import coen268.mycookbook.CookingSession;
import coen268.mycookbook.Dao;
import coen268.mycookbook.FileLocation;
import coen268.mycookbook.R;
import coen268.mycookbook.Recipe;
import coen268.mycookbook.SessionStepInfo;
import coen268.mycookbook.ShareActivity;
import coen268.mycookbook.SharedModel;
import coen268.mycookbook.Util;
import coen268.mycookbook.ui.dummy.CookingSessionDialog;
import static coen268.mycookbook.SharedModel.*;

/**
 * Created by Mantu on 20-05-2015.
 */

public class RecipeView extends FragmentActivity implements OnFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter pagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager viewPager;
    private Dao dao;
    private TextView title;
    public static boolean ActivityInForeGround;
    private Menu menu;
    private ImageButton previewBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_view);
        dao = Dao.getInstance(this);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        pagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);

        title = (TextView)findViewById(R.id.title_text);
        title.setVisibility(View.GONE);
        setTitle(getSelectedRecipe().getTitle());
        previewBtn = (ImageButton) findViewById(R.id.preview);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_recipe_view, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.start_session) {
            startCookingSession(item);
            return true;
        }else if(id == R.id.abort_session) {
            //abort the session
//            int rows = dao.deleteSession(getSessionId());
            setSessionId(-1);
//            if(rows>0) {
            item.setVisible(false);
            menu.findItem(R.id.start_session).setVisible(true);
            Toast.makeText(this, "Active Session Cleared ", Toast.LENGTH_LONG).show();

            title.setVisibility(View.GONE);
//                title.setText("UNTITLED SESSION. Press '+' to start a session");
//            }else {
//                Toast.makeText(this,"Unable to delete session - "+getSessionId(),Toast.LENGTH_LONG).show();
//            }
            previewBtn.setVisibility(View.INVISIBLE);
            menu.findItem(R.id.menu_item_share).setVisible(false);
            menu.findItem(R.id.share_on_FB).setVisible(false);
        }else if(id == R.id.view_sessions) {
            Intent i = new Intent(this,SessionListActivity.class);
            i.putExtra("recipeId",getSelectedRecipe().getId());
            startActivity(i);
        }
        else if(id == R.id.menu_item_share){
            Intent intent = new Intent(this, ShareActivity.class);
            intent.putExtra("sessionId",SharedModel.getSessionId());
            intent.putExtra("SHARE_TYPE", "all");
            startActivity(intent);
        }
        else if(id == R.id.share_on_FB){
            Intent intent = new Intent(this, ShareActivity.class);
            intent.putExtra("sessionId",SharedModel.getSessionId());
            intent.putExtra("SHARE_TYPE", "");
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String fragmentName, Object... obj) {
        //TODO: Handle Events from fragment
    }


    public void startCookingSession(MenuItem v) {
        final EditText view = new EditText(this);
        final Recipe selectedRecipe = getSelectedRecipe();
        view.setText(selectedRecipe.getTitle()+"_"+new Date().toString());

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String text = view.getText().toString();
                CookingSession session = dao.beginSession(selectedRecipe.getId(), text);
                if(session.getSessionId()>0) {
                    title.setVisibility(View.VISIBLE);
                    title.setText(session.getName());
                    SharedModel.setSessionId(session.getSessionId());
                    SharedModel.setSessionReference(session);
                    setSessionId(session.getSessionId());
                    menu.findItem(R.id.abort_session).setVisible(true);
                    menu.findItem(R.id.start_session).setVisible(false);
                    menu.findItem(R.id.menu_item_share).setVisible(true);
                    menu.findItem(R.id.share_on_FB).setVisible(true);
                    previewBtn.setVisibility(View.VISIBLE);
                }
            }
        };
        CookingSessionDialog d = new CookingSessionDialog(this,listener,view);
        d.show(getFragmentManager(),"cooking-session");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case Constants.CAPTURE_IMAGE:
                if(RESULT_OK==resultCode) {
                    //store the image
                    Bundle b = data.getExtras();
                    String path = b.getString(Constants.KEY_IMG_PATH);
                    Long sessionId = b.getLong(Constants.KEY_SESSION_ID);
                    Long recipeId = b.getLong(Constants.KEY_RECIPE_ID);
                    Long stepId = b.getLong(Constants.KEY_STEP_ID);
                    SessionStepInfo.Type t = SessionStepInfo.Type.IMAGE;
                    SessionStepInfo info = new SessionStepInfo();
                    info.setSessionId(sessionId);
                    info.setStepId(stepId);
                    info.setFileLocation(path);
                    info.setType(t);
                    info.setLocationType(FileLocation.SD_CARD);
                    dao.updateSession(info);
                    b.getString("filename");

                }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ActivityInForeGround = true;
        Intent intent = getIntent();
        if(intent != null){
            String action = intent.getAction();
            if(action != null && action == "OPEN_TAB3"){
                viewPager.setCurrentItem(2);
            }
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        ActivityInForeGround = false;
    }

    public void showSessionPreview(View v){
        CookingSession session = SharedModel.getSessionReference();
        Map data = Util.getInstance().getReportData(this, session);
        String url="";
        try {
            url=Util.getInstance().copyData(data, session.getName());
        } catch (Exception e) {
            Toast.makeText(this,"Error generating preview",Toast.LENGTH_SHORT).show();
        }
        //WebView view = new WebView(this);
        Uri parse = Uri.parse("file://"+url + "/index.html");
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType(parse, "text/html");
        startActivity(i);
    }

    @Override
    public void invalidateOptionsMenu(){

    }


}


