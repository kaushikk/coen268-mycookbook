package coen268.mycookbook.ui.renderer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import coen268.mycookbook.Constants;
import coen268.mycookbook.CookingStep;
import coen268.mycookbook.ImageCaptureActivity;
import coen268.mycookbook.R;
import coen268.mycookbook.SharedModel;
import coen268.mycookbook.TimerHandler;
import coen268.mycookbook.Util;
import coen268.mycookbook.ui.NoteOnClickListener;
import coen268.mycookbook.ui.RecipeView;

import static coen268.mycookbook.Constants.*;
/**
 * Created by kaushik on 5/26/15.
 */
public class StepsListItem extends ArrayAdapter<CookingStep> {
    private final List<CookingStep> steps;
    private final Activity activity;
    private final int resourceId;
    private final int TIMER_UPDATE = 1;
    private final int TIMER_COMPLETED = 2;
    private TimerHandler timerHandler;
    boolean timerRunning = false;

    public StepsListItem(Context context, int resource, List<CookingStep> objects) {
        super(context, resource, objects);
        this.steps = objects;
        this.activity = (Activity) context;
        this.resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CookingStep step = steps.get(position);
        LayoutInflater inflator = activity.getLayoutInflater();
        View row = inflator.inflate(resourceId, parent, false);
        ImageButton note = (ImageButton) row.findViewById(R.id.steps_note_btn);
        final ImageButton timer = (ImageButton) row.findViewById(R.id.steps_timer_btn);
        TextView txt = (TextView)row.findViewById(R.id.steps_info_text);
        txt.setText(step.getDescription());
        TextView timetkn = (TextView)row.findViewById(R.id.steps_timetkn_text);
        if(step.getTimeTaken() > 0)
            timetkn.setText(String.valueOf((step.getTimeTaken()))+" minutes");
        else
            timer.setVisibility(View.INVISIBLE);

        timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageButton btn = (ImageButton)v;
                if(btn.isSelected()){
                    timerHandler.stopTimer();
                    btn.setSelected(false);
                    btn.setImageResource(R.drawable.icon_timer);
                    TextView textView = (TextView)activity.findViewById(R.id.remainingTime);
                    textView.setVisibility(View.INVISIBLE);
                    ImageButton button = (ImageButton)activity.findViewById(R.id.stop_timer_btn);
                    button.setVisibility(View.INVISIBLE);
                    timerRunning = false;
                }else{
                    if(timerRunning) {
                        Toast.makeText(activity, "A timer is already running",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        timerHandler= new TimerHandler(mHandler);
                        timerHandler.startTimer(step.getTimeTaken());
                        btn.setSelected(true);
                        btn.setImageResource(R.drawable.stoptimer);
                        TextView textView = (TextView) activity.findViewById(R.id.remainingTime);
                        textView.setVisibility(View.VISIBLE);
                        ImageButton button = (ImageButton) activity.findViewById(R.id.stop_timer_btn);
                        button.setVisibility(View.VISIBLE);
                        timerRunning = true;
                    }
                }
            }
        });

        final long recipeId = SharedModel.getSelectedRecipe().getId();
        final long sessionId = SharedModel.getSessionId();
        note.setOnClickListener(new NoteOnClickListener(activity, recipeId,step,sessionId));

        //setup camera
        ImageButton cam = (ImageButton) row.findViewById(R.id.steps_cam_btn);
        cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imageName = recipeId + "_" + sessionId + "_" + step.getId();
                String path = Constants.IMG_STORE_FOLDER;
                Bundle b = Util.getDefaultBundle(recipeId, sessionId, step.getId());
                b.putString(KEY_IMG_FILENAME, imageName);
                b.putString(KEY_IMG_STORE, path);
                //capture Image
                Intent intent = new Intent(activity, ImageCaptureActivity.class);
                intent.putExtras(b);
                activity.startActivityForResult(intent, Constants.CAPTURE_IMAGE);
                // Once activity succeeded, RecipeView
            }
        });
        if(sessionId > 0){
            cam.setVisibility(View.VISIBLE);
            note.setVisibility(View.VISIBLE);
        }
        final ImageButton button = (ImageButton)activity.findViewById(R.id.stop_timer_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        return row;
    }

    Handler mHandler = new Handler(){
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIMER_COMPLETED:
                    if (RecipeView.ActivityInForeGround)
                        displayAlertMessage();
                    else
                        displayNotification();
                case TIMER_UPDATE:
                    if (RecipeView.ActivityInForeGround)
                        displayTimerUpdate(msg.arg1);
            }
        }
    };

    public void displayAlertMessage(){
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        final Ringtone r = RingtoneManager.getRingtone(activity, notification);
        r.play();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setTitle("Time is up!");
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        r.stop();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        ImageButton button = (ImageButton)activity.findViewById(R.id.stop_timer_btn);
        button.setVisibility(View.INVISIBLE);
        TextView textView = (TextView)activity.findViewById(R.id.remainingTime);
        textView.setVisibility(View.INVISIBLE);
        notifyDataSetChanged();
        timerRunning = false;
    }

    public void displayNotification(){
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(activity)
                        .setSmallIcon(R.mipmap.timerimage)
                        .setContentTitle("MyCookBook Notification")
                        .setContentText("Timer Alert");


        Intent notificationIntent = new Intent(activity, RecipeView.class);
        notificationIntent.setAction("OPEN_TAB3");
        PendingIntent contentIntent = PendingIntent.getActivity(activity, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);
        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(true);
        builder.setLights(Color.BLUE, 500, 500);
        long[] pattern = {500,500,500,500,500,500,500,500,500};
        builder.setVibrate(pattern);
        builder.setStyle(new NotificationCompat.InboxStyle());
        NotificationManager manager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

    public void displayTimerUpdate(int remainingTime){
        TextView textView = (TextView)activity.findViewById(R.id.remainingTime);
        textView.setText(String.valueOf(remainingTime) + " mins remaining");
    }
}
