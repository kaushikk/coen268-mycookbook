package coen268.mycookbook.ui.dummy;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Date;

import coen268.mycookbook.Dao;
import coen268.mycookbook.Recipe;
import coen268.mycookbook.SharedModel;
import coen268.mycookbook.ui.RecipeView;

/**
 * Created by kaushik on 5/21/15.
 */
public class CookingSessionDialog extends DialogFragment {

    private  View view;
    private  DialogInterface.OnClickListener listener;
    private RecipeView context;
    private  Dao dao;

    public CookingSessionDialog() {

    }
    //TODO - FIX THIS DESIGN
    public CookingSessionDialog(RecipeView ctx,DialogInterface.OnClickListener listener, View view) {
        this();
        context = ctx;
        dao = Dao.getInstance(context);
        this.listener = listener;
        this.view = view;
    }

    @Override
    public void setArguments(Bundle args) {
//        args.get
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle("Cooking Session");
        builder.setMessage("Begin Cooking Session");

        builder.setView(view);
        builder.setPositiveButton("Begin", listener);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return builder.create();
    }


}