package coen268.mycookbook.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Locale;

import coen268.mycookbook.R;
import coen268.mycookbook.ui.fragments.CookingStepsFragment;
import coen268.mycookbook.ui.fragments.IngredientListFragment;
import coen268.mycookbook.ui.fragments.PlaceholderFragment;
import coen268.mycookbook.ui.fragments.RecipeDetailFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Fragment fragment;
        switch(position) {
            case 0:
                fragment = RecipeDetailFragment.newInstance(null, null);
                break;
            case 1:
                fragment = IngredientListFragment.newInstance(null, null);
                break;
            case 2:
                fragment = CookingStepsFragment.newInstance(null, null);
                break;
            default:
                fragment = PlaceholderFragment.newInstance(position + 1);
        }
        fragment.setHasOptionsMenu(true);
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return "Description".toUpperCase(l);
            case 1:
                return "Ingredients".toUpperCase(l);
            case 2:
                return "Steps".toUpperCase(l);
        }
        return null;
    }
}
