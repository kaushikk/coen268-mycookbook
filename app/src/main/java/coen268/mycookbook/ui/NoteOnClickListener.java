package coen268.mycookbook.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import coen268.mycookbook.CookingStep;
import coen268.mycookbook.Dao;
import coen268.mycookbook.SessionStepInfo;
import coen268.mycookbook.SharedModel;

/**
 * Created by kaushik on 5/26/15.
 */
public class NoteOnClickListener implements View.OnClickListener, View.OnLongClickListener {

    private long session;
    private long recipeId;
    private CookingStep step;
    private Context ctx;

    public  NoteOnClickListener(Context c, long recipeId, CookingStep step, long sessionId) {
        this.recipeId = recipeId;
        this.step = step;
        this.session = sessionId;
        this.ctx  = c;
    }

    @Override
    public void onClick(View v) {
        // Create a new Note
        final EditText view = new EditText(ctx);

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(view).setTitle("Add Note").setMessage("Add a new Note to the cooking step")
        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String note = view.getText().toString();
                Dao dao = Dao.getInstance(ctx);
                SessionStepInfo info = new SessionStepInfo();
                info.setType(SessionStepInfo.Type.TEXT_NOTE);
                info.setStepId(step.getId());
                info.setSessionId(session);
                info.setFileLocation(note);
                dao.updateSession(info);

            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    @Override
    public boolean onLongClick(View v) {
        //Show all notes
        Bundle b = new Bundle();
        b.putLong("recipeId",recipeId);
        b.putLong("sessionId", session);
        b.putLong("stepId", step.getId());
        Dao d = Dao.getInstance(ctx);
        List<SessionStepInfo> l = d.getSessionSteps(session, SessionStepInfo.Type.TEXT_NOTE);
        return false;
    }
}
