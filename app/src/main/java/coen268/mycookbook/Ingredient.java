package coen268.mycookbook;

/**
 * Created by kaushik on 5/19/15.
 */
public class Ingredient {
    private long id;
    private long recipeId;
    private String name;
    private String quantity;
    private String imgLocation;

    public Ingredient(String name, String quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public Ingredient(long recipeId, long id, String name, String quantity) {
        this.id = id;
        this.recipeId = recipeId;
        this.name = name;
        this.quantity = quantity;
    }

    public Ingredient(long recipeId, long id, String name, String quantity, String imgLocation) {
        this(recipeId,id,name,quantity);
        this.imgLocation = imgLocation;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(long recipeId) {
        this.recipeId = recipeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getImgLocation() {
        return imgLocation;
    }

    public void setImgLocation(String imgLocation) {
        this.imgLocation = imgLocation;
    }
}
