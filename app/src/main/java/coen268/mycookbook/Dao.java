package coen268.mycookbook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static coen268.mycookbook.Constants.*;
/**
 * Created by kaushik on 5/19/15.
 */
public class Dao extends SQLiteOpenHelper{

    public static final String DB_NAME = "RECIPEDB";

    public static final String TABLE_RECIPE = "recipe";
    public static final String TABLE_INGREDIENTS = "ingredients";
    public static final String TABLE_STEPS = "steps";
    public static final String TABLE_COOKING_SESSION = "cooking_session";
    public static final String TABLE_SESSION_IMG_STEPS = "session_steps";
    public static final int DB_VERSION = 1;
    private static boolean init;
    private static  Dao instance;

    private SQLiteDatabase db;
    private Context context;



    private Dao(Context ctx) {
        this(ctx,DB_NAME,null,1);
        this.context=ctx;
    }

    private Dao(Context ctx, int version) {
        this(ctx,DB_NAME,null, version);
        this.context=ctx;

    }

    public static boolean isInitialized() {
        return init;
    }

    public static Dao getInstance(Context context) {
        if(instance==null) {
            instance = new Dao(context);
            instance.db = instance.getWritableDatabase();
        }

        return instance;
    }

    private Dao(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private Dao(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }



    public Recipe createRecipe(Recipe r) {

        //add recipe row first
        ContentValues values = new ContentValues();
        values.put(RECIPE_TITLE,r.getTitle());
        values.put(RECIPE_DESCRIPTION, r.getDescription());
        if(r.getLastUpdated()!=null) {
            values.put(RECIPE_LAST_UPDATED,r.getLastUpdated().getTime());
        }
//        SQLiteDatabase db = getWritableDatabase();
        long recipeId = db.insert(TABLE_RECIPE,null,values);
        r.setId(recipeId);
        if(r.getIngredients()!=null) {
            createIngredients(r, db, recipeId);
        }
        if(r.getCookingSteps()!=null) {
            createSteps(r, db, recipeId);
        }
        Log.i(getClass().getCanonicalName(),"RECIPE CREATED "+recipeId);
        return r;
    }

    private void createSteps(Recipe r, SQLiteDatabase db, long recipeId) {
        ContentValues values;
        for (CookingStep step : r.getCookingSteps()) {
            values = new ContentValues();
            values.put(STEPS_PARENT_RECIPE, recipeId);
            values.put(STEPS_DESCRIPTION, step.getDescription());
            values.put(STEPS_TIME_TAKEN, step.getTimeTaken());
            long stepId = db.insert(TABLE_STEPS, null, values);
            Log.i(getClass().getCanonicalName(),"Step inserted "+stepId);
            step.setId(stepId);
            step.setRecipeId(recipeId);
        }
    }

    private void createIngredients(Recipe r, SQLiteDatabase db, long recipeId) {
        ContentValues values;
        for (Ingredient ingr : r.getIngredients()) {
            values = new ContentValues();
            values.put(INGREDIENTS_PARENT_RECIPE, recipeId);
            values.put(INGREDIENTS_NAME, ingr.getName());
            values.put(INGREDIENTS_QUANTITY, ingr.getQuantity());

            if (ingr.getImgLocation() != null)
                values.put(INGREDIENTS_IMG_LOC, ingr.getImgLocation());
            long ingrId = db.insert(TABLE_INGREDIENTS, null, values);
            Log.i(getClass().getCanonicalName(),"ingredient added "+ingrId);
            ingr.setId(ingrId);
            ingr.setRecipeId(recipeId);
        }
    }

    public List<Recipe> getAllRecipes(){
        String groupBy=null;
        String orderBy=null;
        String having = null;
        String select=null;
        String selectArgs[] = null;
        String columns[] = {RECIPE_ID,RECIPE_TITLE,RECIPE_DESCRIPTION};
//        SQLiteDatabase db = getWritableDatabase();
        List<Recipe> list=new ArrayList<>();
        Cursor c = db.query(TABLE_RECIPE,columns, null,selectArgs,groupBy,having,orderBy);
        while(c.moveToNext()) {
            Recipe r = new Recipe(c.getLong(0),c.getString(1),c.getString(2));
            list.add(r);
            //get Steps for a recipe
            List<CookingStep> steps = getSteps(r.getId());
            r.setCookingSteps(steps);


            //get Ingredient
            List<Ingredient> ingList = getIngredients(r.getId());
            r.setIngredients(ingList);
        }
        return list;
    }

    public List<CookingStep> getSteps(long recipeId) {
        String groupBy=null;
        String orderBy="steps_id";
        String having = null;
        String select=STEPS_PARENT_RECIPE+ "=?";
        String selectArgs[] = {""+recipeId};
        String columns[] = {STEPS_ID,STEPS_PARENT_RECIPE,STEPS_TIME_TAKEN,STEPS_DESCRIPTION}; //TODO update for steps table
        SQLiteDatabase db = getWritableDatabase();
        ArrayList<CookingStep> list = new ArrayList<>();
        Cursor c = db.query(TABLE_STEPS,columns,select,selectArgs,groupBy,having,orderBy);
        while(c.moveToNext()) {
            CookingStep step = new CookingStep(c.getLong(1),c.getLong(0),c.getString(3));
            step.setTimeTaken(c.getLong(2));
            list.add(step);
        }
        return list;
    }

    public List<Ingredient> getIngredients(long recipeId) {
        String groupBy=null;
        String orderBy=null;
        String having = null;
        String select=INGREDIENTS_PARENT_RECIPE+ "=?";
        String selectArgs[] =  {""+recipeId};
        String columns[] = {INGREDIENTS_PARENT_RECIPE,INGREDIENTS_ID,
                INGREDIENTS_NAME,INGREDIENTS_QUANTITY, INGREDIENTS_IMG_LOC}; //TODO update for Ingredient table
        SQLiteDatabase db = getWritableDatabase();
        ArrayList<Ingredient> list = new ArrayList<>();
        Cursor c = db.query(TABLE_INGREDIENTS,columns,select,selectArgs,groupBy,having,orderBy);
        while(c.moveToNext()) {
            Ingredient i = new Ingredient(c.getLong(0),c.getLong(1),c.getString(2),c.getString(3),c.getString(4));
            list.add(i);
        }
        return list;
    }

    public Recipe getRecipeById(long id) {
        Recipe recipe=null; //get receipe from DB
        String groupBy=null;
        String orderBy=null;
        String having = null;
        String select=RECIPE_ID+"=?";
        String selectArgs[] = {""+id};
        String columns[] = {RECIPE_ID,RECIPE_TITLE,RECIPE_DESCRIPTION};
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.query(TABLE_RECIPE,columns, select,selectArgs,groupBy,having,orderBy);
        while(c.moveToNext()) {
            recipe = new Recipe(c.getLong(0),c.getString(1),c.getString(2));
            //get Steps for a recipe
            List<CookingStep> steps = getSteps(recipe.getId());
            recipe.setCookingSteps(steps);
            //get Ingredient
            List<Ingredient> ingList = getIngredients(recipe.getId());
            recipe.setIngredients(ingList);
            break;
        }
        return recipe;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        /*
        FIXME - There are problems creating the entire schema using the script create.db.
        below code is a workaround
       */
        int table_id = R.raw.create_recipe;
        boolean created = createTable(db, table_id);
        if(!created) {
            throw new RuntimeException("UNABLE TO CREATE RECIPE TABLE ");
        }
        created = createTable(db,R.raw.create_ingredients);
        if(!created) {
            throw new RuntimeException("UNABLE TO CREATE INGREDIENTS TABLE ");
        }
        created = createTable(db,R.raw.create_steps);
        if(!created) {
            throw new RuntimeException("UNABLE TO CREATE STEPS TABLE ");
        }
        created = createTable(db,R.raw.create_session);
        if(!created) {
            throw new RuntimeException("UNABLE TO CREATE SESSIONS TABLE ");
        }
        created = createTable(db,R.raw.create_session_steps);
        if(!created) {
            throw new RuntimeException("UNABLE TO CREATE SESSIONS-STEP TABLE ");
        }

        init=created;

    }

    private boolean createTable(SQLiteDatabase db, int table_id) {
        InputStreamReader reader = new InputStreamReader(context.getResources().openRawResource(table_id));
        BufferedReader br = new BufferedReader(reader);
        StringBuilder b = new StringBuilder();
        String s="";
        try {
            while((s=br.readLine())!=null) {
                if(s.trim().startsWith("--")) {
                    continue;
                }
                b.append(s).append("\n");
            }

            db.execSQL(b.toString());

        } catch(Exception e) {
            Log.e("DB CREATE",e.getMessage());
//            return false;

        }
        return true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void resetDb() {
        db.execSQL("DROP TABLE SESSION_STEPS;");
        db.execSQL("DROP TABLE COOKING_SESSION;");
        db.execSQL("DROP TABLE STEPS;");
        db.execSQL("DROP TABLE INGREDIENTS;");
        db.execSQL("DROP TABLE RECIPE;");
        onCreate(this.db);
    }

    public CookingSession beginSession(long recipeId, String name) {
        ContentValues values = new ContentValues();
        values.put(SESSION_PARENT_RECIPE, recipeId);
        values.put(SESSION_NAME, name);
//        values.put(SESSION_CREATED_DATE,System.currentTimeMillis());
        long id = db.insert(TABLE_COOKING_SESSION,null,values);
        if(id>0) {
            return new CookingSession(id,recipeId,name);
        }else {
            return null;
        }
    }

    public SessionStepInfo updateSession(SessionStepInfo info) {
        ContentValues values = new ContentValues();
        values.put(SESSION_IMG_STEP_ID,info.getStepId());
        values.put(SESSION_IMG_SESSION_ID,info.getSessionId());
        values.put(SESSION_IMG_FILE_LOC,info.getFileLocation());
        values.put(SESSION_IMG_TYPE,info.getType().toString());
        if(info.getLocationType()!=null) {
            values.put(SESSION_IMG_FILE_LOC_TYPE, info.getLocationType().value());
        }
        long id = db.insert(TABLE_SESSION_IMG_STEPS, null, values);
        return info;

    }
    public SessionStepInfo deleteSessionStep(SessionStepInfo info) {
        ContentValues values = new ContentValues();
        values.put(SESSION_IMG_STEP_ID,info.getStepId());
        values.put(SESSION_IMG_SESSION_ID,info.getSessionId());
        values.put(SESSION_IMG_FILE_LOC,info.getFileLocation());
        values.put(SESSION_IMG_TYPE,info.getType().toString());
        values.put(SESSION_IMG_FILE_LOC_TYPE,info.getLocationType().toString());
        String where = SESSION_IMG_STEP_ID+"=?"+" and "+SESSION_IMG_SESSION_ID+" = ? "+" and " + SESSION_IMG_FILE_LOC+" = ? ";
        String[] whereArgs={info.getStepId()+"",info.getSessionId()+"",info.getFileLocation()};
        long id = db.delete(TABLE_SESSION_IMG_STEPS, where, whereArgs);
        return info;

    }

    /**
     * Get the steps information for notes or images
     * @param sessionId
     * @param t
     * @return
     */
    public List<SessionStepInfo> getSessionSteps(long sessionId, SessionStepInfo.Type t) {
        String[] cols = {SESSION_IMG_SESSION_ID,SESSION_IMG_STEP_ID,SESSION_IMG_ID,SESSION_IMG_TYPE,SESSION_IMG_FILE_LOC};
        String selection="session_id=? and "+SESSION_IMG_TYPE+"="+ t.toString();
        String[] selectionArgs = {""+sessionId};
        List<SessionStepInfo> list = new ArrayList<>();
        Cursor c  = db.query(TABLE_SESSION_IMG_STEPS, cols, selection, selectionArgs, null, null, null);
        while(c.moveToNext()) {
            SessionStepInfo info = new SessionStepInfo();
            info.setSessionId(c.getLong(0));
            info.setStepId(c.getLong(1));
            info.setId(c.getLong(2));
            info.setType(t);
            info.setFileLocation(c.getString(4));
            list.add(info);
        }

        return list;
    }

    /**
     * Get the steps information for notes or images
     * @param sessionId
     * @return
     */
    public List<SessionStepInfo> getSessionSteps(long sessionId) {
        String[] cols = {SESSION_IMG_SESSION_ID,SESSION_IMG_STEP_ID,SESSION_IMG_ID,SESSION_IMG_TYPE,SESSION_IMG_FILE_LOC};
        String selection="session_id=?";
        String[] selectionArgs = {""+sessionId};
        List<SessionStepInfo> list = new ArrayList<>();
        Cursor c  = db.query(TABLE_SESSION_IMG_STEPS,cols, selection,selectionArgs,null,null,SESSION_IMG_STEP_ID);
        while(c.moveToNext()) {
            SessionStepInfo info = new SessionStepInfo();
            info.setSessionId(c.getLong(0));
            info.setStepId(c.getLong(1));
            info.setId(c.getLong(2));
            if(SessionStepInfo.Type.IMAGE.toString().equalsIgnoreCase(c.getString(3))) {
                info.setType(SessionStepInfo.Type.IMAGE);
            }else {
                info.setType(SessionStepInfo.Type.TEXT_NOTE);
            }
            info.setFileLocation(c.getString(4));
            list.add(info);
        }

        return list;
    }

    public int deleteSession(long sessionId){
        String[] whereArgs = {sessionId + ""};
        int rows = db.delete(TABLE_SESSION_IMG_STEPS, "session_id=?", whereArgs);
        return db.delete(TABLE_COOKING_SESSION,"session_id=?",whereArgs);
    }

    public List<CookingSession> getSessionByRecipeId(long recipeId) {
        String groupBy=null;
        String orderBy=null;
        String having = null;
        String select=SESSION_PARENT_RECIPE+ "=?";
        String selectArgs[] =  {""+recipeId};

        if(recipeId<=0) {
            select=null;
            selectArgs=null;
        }

        String columns[] = {SESSION_ID,SESSION_PARENT_RECIPE,
                SESSION_NAME };
        Cursor c = db.query(TABLE_COOKING_SESSION, columns, select, selectArgs, groupBy, having, orderBy);
        List<CookingSession> list = new ArrayList<>();
        while(c.moveToNext()) {
            CookingSession s = new CookingSession(c.getLong(0),c.getLong(1),c.getString(2));
            list.add(s);
        }
        return list;
    }

    public List<CookingSession> getSessionById(long recipeId) {
        String groupBy=null;
        String orderBy=null;
        String having = null;
        String select=SESSION_ID+ "=?";
        String selectArgs[] =  {""+recipeId};

        if(recipeId<=0) {
            select=null;
            selectArgs=null;
        }

        String columns[] = {SESSION_ID,SESSION_PARENT_RECIPE,
                SESSION_NAME };
        Cursor c = db.query(TABLE_COOKING_SESSION, columns, select, selectArgs, groupBy, having, orderBy);
        List<CookingSession> list = new ArrayList<>();
        while(c.moveToNext()) {
            CookingSession s = new CookingSession(c.getLong(0),c.getLong(1),c.getString(2));
            list.add(s);
        }
        return list;
    }
}
