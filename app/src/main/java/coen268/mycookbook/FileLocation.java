package coen268.mycookbook;

/**
 * Created by kaushik on 5/19/15.
 */
public enum FileLocation {
    DROPBOX(0),LOCAL_FILE_SYSTEM(1), SD_CARD(2),OTHER(3);


    int i;

    FileLocation(int i ) {
        this.i=i;
    }

    public int value() {
        return i;
    }


}
