package coen268.mycookbook;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

public class DatabaseTest extends AndroidTestCase {
    private Dao dao;
    private RenamingDelegatingContext context;


    @Override
    public void setUp() throws Exception {
        super.setUp();
//        RenamingDelegatingContext context = new RenamingDelegatingContext(getActivity(), "test_");
        context = new RenamingDelegatingContext(getContext(), "test");
        dao = Dao.getInstance(context);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    //According to Zainodis annotation only for legacy and not valid with gradle>1.1:
    //@Test
    public void testAddEntry(){
        Recipe r = new Recipe("Pressure Cook RICE","Instructions on how to cook rice");
        r.setLastUpdated(new Date(System.currentTimeMillis()));
        Ingredient i = new Ingredient("Indian Rice","1 cup");
        r.addIngredient(i);
        r.addIngredient(new Ingredient("Water", "2.9 Cups"));

        CookingStep step = new CookingStep("Pressure Cook Rice and Water for 4 whistles",20);
        r.addCookingStep(step);
        r.addCookingStep(new CookingStep("Let the rice cool before opening", 10));
        Long recipeId= dao.createRecipe(r).getId();
        Log.i("ADD RECIPE", "recipeID=" + recipeId);

        r = new Recipe("Cucumber Stir Fry","Quick and Easy Curry. Can be prepared in 5 minutes, especially when short of time");
        r.setLastUpdated(new Date(System.currentTimeMillis()));
        i = new Ingredient("Organic Cucumber","1 lb");
        r.addIngredient(i);
        r.addIngredient(new Ingredient("Sambhar Powder", "1 tsp"));
        r.addIngredient(new Ingredient("Cooking Oil", "2 tsp"));
        r.addIngredient(new Ingredient("Cumin Seeds", "1/4 tsp"));
        r.addIngredient(new Ingredient("Split Urad Dal", "1 tsp"));
        r.addIngredient(new Ingredient("Turmeric Powder", "1/4 tsp"));
        r.addIngredient(new Ingredient("Salt", "As per taste"));


        step = new CookingStep("Chop cucumber to small pieces",5);
        r.addCookingStep(step);
        r.addCookingStep(new CookingStep("Heat Oil in a heavy bottomed pan",10));
        r.addCookingStep(new CookingStep("Add Cumin Seeds, Split Urad Dal and saute",1));
        r.addCookingStep(new CookingStep("Add turmeric powder",1));
        r.addCookingStep(new CookingStep("Add Cucumber, Sambhar Powder and Salt",1));
        r.addCookingStep(new CookingStep("Saute",1));
        r.addCookingStep(new CookingStep("Close the pan and let it cook for 5 minutes",5));
        recipeId= dao.createRecipe(r).getId();
        Log.i("ADD RECIPE", "recipeID=" + recipeId);

        checkRecipe(r, recipeId);
    }

    private void checkRecipe(Recipe r, Long recipeId) {
        assertNotNull(recipeId);
        assertNotSame(-1, recipeId);

        for(Ingredient ingredient : r.getIngredients()) {
            assertNotSame(-1, ingredient.getId());
            assertSame(recipeId,ingredient.getRecipeId());
        }

        for(CookingStep cs: r.getCookingSteps()) {
            assertNotSame(-1, cs.getId());
            assertSame(recipeId,cs.getRecipeId());
        }
    }

    public void testListRecipe() {

        List<Recipe> recipes= dao.getAllRecipes();
        assertNotNull(recipes);
        assertTrue(recipes.size() > 0);
        PrintWriter log=null;
        try {
            FileOutputStream fs = context.openFileOutput("log.txt", Context.MODE_PRIVATE);
            log = new PrintWriter(new OutputStreamWriter(fs));

            for(Recipe r: recipes) {
                log.println("recipe: " + r.getId() + " " + r.getTitle() + r.getDescription());
                log.flush();
                assertNotNull(r.getIngredients());
                assertTrue(r.getIngredients().size() > 1);
                assertNotNull(r.getCookingSteps());
                assertTrue(r.getCookingSteps().size()>=2);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // simulate Cooking Session & Steps

    public void testCookingSessionCreate() {
        List<Recipe> recipes = dao.getAllRecipes();
        assertNotNull(recipes);
        assertTrue(recipes.size() > 0);
        Recipe r = recipes.get(0);
        CookingSession session = dao.beginSession(r.getId(),"Test Session ");
        assertTrue(session.getSessionId()>0);
         session = dao.beginSession(r.getId(),"Test Session 2 ");
        assertTrue(session.getSessionId()>0);
         session = dao.beginSession(r.getId(),"Test Session 3");
        assertTrue(session.getSessionId()>0);
    }

     public void testSessionStepUpdate() {
         //recipeid =1 , session id =1, stepid =2
         SessionStepInfo info = new SessionStepInfo();
         info.setSessionId(1);
         info.setStepId(2);
         info.setFileLocation("file:///sdcard/images/image.jpg");
         info.setLocationType(FileLocation.SD_CARD);
         info.setType(SessionStepInfo.Type.IMAGE);
         dao.updateSession(info);
     }
}