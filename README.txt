README file for Cookbook Project

---MILESTONE 1 - 20 May 2015----
The following functionalities are available. The time for implementation are also shown
------
DESIGN
------
Data Model Design and Implementation (8 hours)

-------
IMPLEMENTATION
-------
1. Capture Image and Store in File ( 1 hour)
2. Dropbox Integration (3 hours)
    - Upload a file
3. Facebook Account Integration (8 hours)
    - Post/Share
    - Like
4. DB Schema for Cookbook (8 hours)
5. Recipe List View (3 hours)
6. Recipe Detail View - Integration
    - Invokes

7. DAO class implementation (8 hours)
8. Junit For DAO classes ( 4 hours)
----

-----------------
Integration Effort - 4 hours
-----------------

Pending Items
-------------
1. UI development - Cooking Session View,
2.  Integration with IMage Capture, FB, Dropbox
3. Swipe Navigation
4. Report Generation as HTML/PDF